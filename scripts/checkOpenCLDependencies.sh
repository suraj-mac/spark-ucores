#!/bin/bash


LibAparapiNameStr="libaparapi_*.so"

MissingDependencyStr="not found"

TestDependencyOutputStr=$(find ../bin/dist.* -name "*.so" | xargs ldd)

echo "------------------------"

echo "$TestDependencyOutputStr"

echo "------------------------"

if [[ $TestDependencyOutputStr =~ $MissingDependencyStr ]]
then
   echo "There are missing dependencies!"
   echo "------------------------"
   echo "The '$LibAparapiNameStr' with missing dependencies are the ones that contain the string: '$MissingDependencyStr'"
   echo "Examine and rebuild only the relevant '$LibAparapiNameStr' for your type of hardware"
   echo "You probably do not need all of the provided '$LibAparapiNameStr' for your type of compute nodes"
   echo "To build a new '$LibAparapiNameStr' go to your aparapi-ucores project, build com.amd.aparapi.jni and copy the new $LibAparapiNameStr' to the proper [SparkCL Home]/bin/std.X"
   echo "Check the documentation for exact details on how to do that!"
else
   echo "No missing dependencies found"
   echo "------------------------"
   echo "All '$LibAparapiNameStr' dependencies are in order"
   echo "You should be good to go!"
fi 
echo "------------------------"

